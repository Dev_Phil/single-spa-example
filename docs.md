# Single-Spa-Example with Angular and React

## If you want to recreate this setup, follow the instruction bellow
1. Install single-spa globally via npm: (npm i -g single-spa)
2. Create Host Application<br>
    2.1. ``` create-single-spa ``` <br>
    2.2. Choose directory (e.g. shell) <br>
    2.3. Choose 'single-spa root config' <br>
    2.4. Choose Package Manager: (e.g. npm) <br>
    2.5. Choose Typescript (y) <br>
    2.6. CHoose single-spa Layout Engine (y) <br>
    2.7. Organization Name: (telekom) <br>
3. Creating a Angular MicroFrontend <br>
    1. ``` create-single-spa ``` <br>
        1. Choose directory (.) <br>
        2. Choose 'single-spa application / parcel' <br>
        3. Choose Framework: (angular) <br>
        4. Choose Project Name (angular-mf) <br>
        5. Choose Angular Routing (y) <br>
        6. Choose Stylesheet format: (Scss) <br>
        7. When asked if you like to proceed, choose (Y) <br>
        8. If you chose Angular Routing enter (Y) <br>
        9. Choose Port (e.g. 4200) <br>
    2. Edit app-routing.module.ts <br>
        1. Add this between the imports and exports:  ```providers: [{provide: APP_BASE_HREF, useValue: '/'}], ``` and import the APP_BASE_HREF from @angular/common
    3. Build your Application:
       1.  ``` npm i ```
       2.  ``` npm run build:single-spa:angular-mf ```
4. Creating a React MicroFrontend <br>
    1. ``` create-single-spa ``` <br>
        1. Choose directory (react-mf) <br>
        2. Choose 'single-spa application / parcel' <br>
        3. Choose Framework: (react) <br>
        4. Choose if you want to use TypeScript (N) <br>
        5. Choose Organization name: (telekom) <br>
        6. Choose a project name: (react-mf) <br>
    2. Build your Application with: ``` npm run build:webpack```
5. Importing your MF's into your Shell App: <br>
   1. In index.js import your Frontends on Line 52: 
       ``` 
          "@telekom/react-mf": "//localhost:8080/telekom-react-mf.js",
          "angular-mf": "//localhost:8081/main.js"
        ```
    2. Before your React Import, add these two lines for the react libaries:
        ``` 
        "react": "https://cdn.jsdelivr.net/npm/react@16.13.1/umd/react.production.min.js",
        "react-dom": "https://cdn.jsdelivr.net/npm/react-dom@16.13.1/umd/react-dom.production.min.js",
        ```
    3. Uncomment Script for Loading Zone.js (Line 61/63)
    4. On line 20, add (data:) behind ``` localhost:*; -> localhost:* data:; ```
    5. Replace microfrontend-layout.html with: 
        ```
        <single-spa-router>
            <main>
                <route path="/angular">
                    <application name="angular-mf"></application>
                </route>
                <route default>
                    <application name="@telekom/react-mf"></application>
                </route>
            </main>
        </single-spa-router>
        ```
    6. Go into the Dist folders of your MF's and run them by typing:
        ``` htttp-server . --cors -p {port (angular = 8081, react = 8080)}```
    7. Go into the Shell App and start it by typing: ```npm start```
6. Check that on localhost:9000, your react App gets Loaded, and on /angular your angular App
   